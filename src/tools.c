#include "../src/tools.h"
#include "malloc.h"
#include <string.h>
#include "tools.h"


//******** ****************************Gestion Création Ctrl*************************
// Fonction pour l'ajout d'un nouveau Ctrl
void F_AddNewCtrl (S_CelluleCtrl** addressListCtrl)
{
    S_Ctrl Ctrl;
    S_CelluleCtrl* cellCtrl;
    

    printf("Veuillez saisir ID du Ctrl (Ex. PLC-101) : ");
    scanf("%s", &(Ctrl.No_Ctrl));

    printf("Veuillez saisir No Secteur (Ex. B100) : ");
    scanf("%s", &(Ctrl.No_Secteur));

    if(F_ExistCtrl(*addressListCtrl,Ctrl.No_Ctrl,Ctrl.No_Secteur))
    {
        printf("Ctrl est déjà dans la Data Base \n ");
        F_PrintCelluleCtrlList(*addressListCtrl);
        return;
    }


    printf("Veuillez saisir le Fabricant (Rockwell = AB , DeltaV = DV) : ");
    scanf("%s", &(Ctrl.Fabricant));

    printf("Veuillez saisir la Version (Ex. 1 ) : ");
    scanf("%d", &(Ctrl.Version));
    Ctrl.EspaceTotal= 10;
    Ctrl.EspaceDisponible = Ctrl.EspaceTotal;
    
   cellCtrl = F_CreateCelluleCtrl();
   F_UpdateCelluleCtrl(Ctrl,cellCtrl);
   F_AddCelluleCtrl(addressListCtrl,cellCtrl);
   
}
// Fonction Allocation Espace mémoire
S_CelluleCtrl * F_CreateCelluleCtrl()
{
    S_CelluleCtrl* cell = (S_CelluleCtrl*)malloc(sizeof(S_CelluleCtrl));
    if(cell != NULL)
    {
        cell->suivant = NULL;
        cell->Instruments = NULL;
    }
    return cell;
}
// Update celluleCtrl
void F_UpdateCelluleCtrl(S_Ctrl Ctrl, S_CelluleCtrl *cell)
{
    cell ->Ctrl = Ctrl;

}
// add celluleCtrl
void F_AddCelluleCtrl(S_CelluleCtrl ** adresseListCtrl,S_CelluleCtrl * CelluleCtrl)
{
     // Si la liste est vide, assigner la cellule directement
     if (*adresseListCtrl == NULL)
     {
        *adresseListCtrl = CelluleCtrl;
        return;
     }

    // Parcourir la liste jusqu'à la dernière cellule
    S_CelluleCtrl *derniereCellule = *adresseListCtrl;
    while (derniereCellule ->suivant !=NULL)
    {
        derniereCellule = derniereCellule ->suivant;
    }
     // Ajouter la nouvelle cellule à la fin de la liste
    derniereCellule ->suivant = CelluleCtrl;

}

// Create celluleInstrument
S_CelluleInstrument * F_CreateCelluleInstrument() {
    S_CelluleInstrument *cell = (S_CelluleInstrument*) malloc(sizeof(S_CelluleInstrument));
    cell->suivant = NULL;
    return cell;
}

// Update celluleInstrument
void F_updateCelluleInstrument(S_Instrument Instrument_1, S_CelluleInstrument *cell){
    cell->Instrument = Instrument_1;
}

// Add celluleInstrument
void F_addCelluleInstrument(S_CelluleInstrument **addrListeInstrument, S_CelluleInstrument *cell) {
    // Si la listeInstrument est vide, la listeInstrument doit pointer vers la cell. 
    // Dans le cas contraire, il convient d'ajouter cell à la fin.

    if (*addrListeInstrument == NULL) {
        *addrListeInstrument = cell;
        return;
    }

    S_CelluleInstrument *currentCell = *addrListeInstrument;
    while (currentCell->suivant != NULL) {
        currentCell = currentCell->suivant;
    }
    currentCell->suivant = cell;
}




// Fonction qui affiche les élements Ctrl dans la BD 
void F_PrintCelluleCtrl(S_CelluleCtrl Cell)
{
    
    printf("           No_Ctrl :  %s      No_Secteur : %s      Fabricant : %s      Version : %d      Espace Disponible : %d     \n", Cell.Ctrl.No_Ctrl,Cell.Ctrl.No_Secteur,Cell.Ctrl.Fabricant,Cell.Ctrl.Version,Cell.Ctrl.EspaceDisponible);
}
//Fonction pour afficher la liste Ctrl.
void F_PrintCelluleCtrlList(S_CelluleCtrl *listeCtrl)
{
    
    S_CelluleCtrl *celluleCourante = listeCtrl;
    printf("*********************************************************************************************************************\n");
    printf("*                                   Liste des Contrôleurs dans la Data Base                                         *\n");
    while (celluleCourante !=NULL)
    {
        F_PrintCelluleCtrl(*celluleCourante);
        celluleCourante = celluleCourante ->suivant;
    }
    printf("*********************************************************************************************************************\n");
}

//Fonction pour affichier un Instrument
void F_printCelluleInstrument(S_CelluleInstrument cell) {
    
    printf("                No Instrument : %s      Range_High : %d      Range_Low : %d      Unit : %s                               \n", 
               cell.Instrument.No_Instrument,cell.Instrument.Range_High,cell.Instrument.Range_Low,cell.Instrument.Unit);
   }

//Fonction pour afficher la liste Ctrl.
void F_PrintCelluleInstrumentlist(S_CelluleCtrl *listeInstrument)
{
    
    S_CelluleInstrument *celluleCourante = listeInstrument;
    printf("*********************************************************************************************************************\n");
    printf("*                                   Liste des Instrument dans la Data Base  ()                                      *\n");
    while (celluleCourante !=NULL)
    {
        F_printCelluleInstrument(*celluleCourante);
        celluleCourante = celluleCourante ->suivant;
    }
    printf("*********************************************************************************************************************\n");
}


// Vérifie l'existence d'un Ctrl dans la liste des Ctrl
int F_ExistCtrl(S_CelluleCtrl *listeCtrl, char *No_Ctrl, char *No_Secteur)
 {
    // Parcourir la liste des Ctrl
    while (listeCtrl != NULL) {
        // Vérifier si le Ctrl correspond aux critères de recherche
        if (strcmp(listeCtrl->Ctrl.No_Ctrl, No_Ctrl) == 0 && strcmp(listeCtrl->Ctrl.No_Secteur, No_Secteur) == 0) 
        {
            return 1; // Le Ctrl existe
        }
        // Passer au Ctrl suivant dans la liste
        listeCtrl = listeCtrl->suivant;
    }
    return 0; // Le Ctrl n'existe pas
}

int F_existInstrument(S_CelluleInstrument *listeInstrument, char No_Instrument [8])
 {
    S_CelluleInstrument *currentCell = listeInstrument;

    while (currentCell != NULL) {
        if (strcmp(currentCell->Instrument.No_Instrument , No_Instrument)==0) {
            return 1; // Instrument exists
        }
        currentCell = currentCell->suivant;
    }

    return 0; // Instrument does not exist
}




void F_InstrumentToCtrl(S_CelluleCtrl *listeCtrl,S_CelluleInstrument **addrListeInstrument)
{
    S_Ctrl Ctrl;
    printf("Veuillez saisir ID du Ctrl (Ex. PLC-101) : ");
    scanf("%s", &(Ctrl.No_Ctrl));

    printf("Veuillez saisir No Secteur (Ex. B100) : ");
    scanf("%s", &(Ctrl.No_Secteur));

    F_AddInstrumentToCtrl(listeCtrl,addrListeInstrument,Ctrl.No_Ctrl,Ctrl.No_Secteur);

}
void F_AddInstrumentToCtrl(S_CelluleCtrl *listeCtrl,S_CelluleInstrument**addrListeInstrument, char *No_Ctrl, char *No_Secteur ) 
{
    // Gestion des exceptions
    if (!F_ExistCtrl(listeCtrl,No_Ctrl,No_Secteur)) {
        printf("Erreur : Le Ctrl : %s dans le Secteur : %s n'existe pas.\nAppuyez sur Entrée pour continuer...", No_Ctrl,No_Secteur);
        getchar(); // Pause to display the error message
        getchar(); // Wait for user to press Enter to proceed
        return;
    }

    S_CelluleCtrl *CtrlChoisi = listeCtrl;
    F_PrintCelluleCtrlList(listeCtrl);
    while (CtrlChoisi !=NULL)
    {
        if(strcmp(CtrlChoisi->Ctrl.No_Ctrl, No_Ctrl) == 0 && strcmp(CtrlChoisi->Ctrl.No_Secteur, No_Secteur) == 0)
        { 
            
            break;
        }

        CtrlChoisi = CtrlChoisi->suivant;    
    }


    
    printf("ctrl choisi:  %s \n",CtrlChoisi->Ctrl.No_Ctrl);



    S_Instrument Instrument ; 
    S_CelluleInstrument *activeInstrument = *addrListeInstrument;
    printf("Entrez le Numéro de l'Instrument : ");
    scanf("%s", &(Instrument.No_Instrument));
    
    if (F_existInstrument(activeInstrument, Instrument.No_Instrument)) {
        printf("F_existe vrai\n"); 
        while (activeInstrument != NULL) {
            if (strcmp(activeInstrument->Instrument.No_Instrument ,Instrument.No_Instrument)==0) {
                break;
            }
            activeInstrument = activeInstrument->suivant;
        }

        printf("L'Instrument est déjà enregistré. Ses informations sont les suivantes :\n");
        printf("*********************************************************************************************************************\n");
        printf("*                                       Liste des Instrument dans la Data Base  ()                                  *\n");
    
        F_printCelluleInstrument(*activeInstrument);
        printf("*********************************************************************************************************************\n");
        //printf(" Appuyez sur Entrée pour annuler.");
        //getchar();
       // getchar();
        }

     else {
        // Il s'agit du cas (b), l'Instrument n'existe pas, et alors nous devons enregistrer ses informations.
        printf("L'instrument n'est pas enregistré. Nous allons procéder à son enregistrement.\n");
        printf("Veuillez entrer les information pour l'instrument suivant : %s \n",Instrument.No_Instrument);
        printf("Veuillez entrer le Range High de l'instrument : ");
        scanf("%d", &(Instrument.Range_High));
        printf("Veuillez entrer le Range Low de l'instrument : ");
        scanf("%d", &(Instrument.Range_Low));
        printf("Veuillez entrer les unitées de l'instrument : ");
        scanf("%s", &(Instrument.Unit));

        // Nous devons d'abord créer l'Instrument.
        S_CelluleInstrument  *CellInstrument = F_CreateCelluleInstrument();
        F_updateCelluleInstrument(Instrument,CellInstrument);
        F_addCelluleInstrument(addrListeInstrument, CellInstrument);
        // Ici, nous utilisons activeInstrument pour nous assurer que nous avons créé ou utilisé un Instrument existant, notre code devrait donc fonctionner de la même manière.
        activeInstrument = CellInstrument;
        
        if(F_addActiveInstrumentToCtrl(&(CtrlChoisi->Instruments),activeInstrument,CtrlChoisi->Ctrl.No_Ctrl,CtrlChoisi->Ctrl.No_Secteur,activeInstrument->Instrument.No_Instrument));{
            CtrlChoisi->Ctrl.EspaceDisponible --;
            S_CelluleCtrlInstrument *ClientCourant = CtrlChoisi->Instruments;
            while (ClientCourant != NULL) {
            ClientCourant = ClientCourant->suivant;
    }
        }

    }

        
}

int F_addActiveInstrumentToCtrl(S_CelluleCtrlInstrument ** addrInstrumentCtrlActif,S_CelluleInstrument *activeInstrument, char No_Ctrl [8], char No_Secteur [5], char No_Instrument [8])
{
    
    S_CelluleCtrlInstrument *p = *addrInstrumentCtrlActif;
    S_CelluleCtrlInstrument *q=NULL;
    while(p!=NULL)
    {       
        if(p->Instrument==activeInstrument){
        return 0;
        }
        q = p;
        p= p->suivant;
       
    }
     
    S_CelluleCtrlInstrument *cellule = (S_CelluleCtrlInstrument*)malloc(sizeof(S_CelluleCtrlInstrument));
    cellule ->suivant = NULL;
    cellule->Instrument = activeInstrument;
    strcpy(cellule->ID_Ctrl, No_Ctrl);
    strcpy(cellule->ID_Instrument, No_Instrument);
    strcpy(cellule->ID_Secteur, No_Secteur);
    if(*addrInstrumentCtrlActif == NULL)
    {
        *addrInstrumentCtrlActif = cellule;
        return 1;
    }

    q->suivant = cellule;
    return 1;
}



void F_save(S_CelluleCtrl *listeCtrl, S_CelluleInstrument* listeInstrument) {
    // Sauvegarde des Instrument
    FILE *fichierInstrument = fopen("db_Instrument.txt", "w");
   
    if (fichierInstrument == NULL) 
    {
        printf("Impossible d'ouvrir le fichier de la base de données des Instruments.\n");
        getchar(); getchar(); // Pause pour afficher le message d'erreur
        return;
    }
    S_CelluleInstrument *InstrumentCourant = listeInstrument;
    
    while (InstrumentCourant != NULL) 
    {
        fprintf(fichierInstrument, "%s %d %d %s\n", InstrumentCourant->Instrument.No_Instrument, InstrumentCourant->Instrument.Range_High, InstrumentCourant->Instrument.Range_Low, InstrumentCourant->Instrument.Unit);
        InstrumentCourant = InstrumentCourant->suivant;
    }
    
    fclose(fichierInstrument);

    // Sauvegarde des Ctrl et des associations Ctrl-Instrument
    FILE *fichierCtrl = fopen("db_Ctrl.txt", "w");
    FILE *fichierCtrlInstrument = fopen("db_CtrlInstrument.txt", "w");
    
    if (fichierCtrl == NULL || fichierCtrlInstrument == NULL) {
        printf("Impossible d'ouvrir le fichier de la base de données des Ctrl.\n");
        getchar(); getchar(); // Pause pour afficher le message d'erreur
        return;
    }
    S_CelluleCtrl *CtrlCourant = listeCtrl;
    
    while (CtrlCourant != NULL) 
    {   fprintf(fichierCtrl,"******************************************************************\n");
        fprintf(fichierCtrl,"No_Ctrl   No_Secteur   Fabricant   Version\n");
        fprintf(fichierCtrl,"******************************************************************\n");
        fprintf(fichierCtrl, "%s   %s        %s   %d %d %d\n", CtrlCourant->Ctrl.No_Ctrl, CtrlCourant->Ctrl.No_Secteur, CtrlCourant->Ctrl.Fabricant, CtrlCourant->Ctrl.Version, CtrlCourant->Ctrl.EspaceTotal, CtrlCourant->Ctrl.EspaceDisponible);
        fprintf (fichierCtrlInstrument,"******************************************************************\n");
        fprintf(fichierCtrlInstrument,"No_Ctrl   No_Secteur   Fabricant   Version\n");
        fprintf(fichierCtrlInstrument,"%s   %s   %s   %d %d %d\n", CtrlCourant->Ctrl.No_Ctrl, CtrlCourant->Ctrl.No_Secteur, CtrlCourant->Ctrl.Fabricant, CtrlCourant->Ctrl.Version, CtrlCourant->Ctrl.EspaceTotal, CtrlCourant->Ctrl.EspaceDisponible);
        fprintf (fichierCtrlInstrument,"******************************************************************\n");
        fprintf(fichierCtrlInstrument,"No_Instrument\n");
        S_CelluleCtrlInstrument *InstrumentCtrlCourant = CtrlCourant->Instruments;
        while (InstrumentCtrlCourant != NULL) {
            fprintf(fichierCtrlInstrument, "%s \n", InstrumentCtrlCourant->Instrument->Instrument.No_Instrument);
            InstrumentCtrlCourant=InstrumentCtrlCourant->suivant;
        }
        CtrlCourant = CtrlCourant->suivant;
        fprintf (fichierCtrl,"\n\n");
        fprintf (fichierCtrlInstrument,"\n\n");
    }
    
    fclose(fichierCtrl);
    fclose(fichierCtrlInstrument);
}


