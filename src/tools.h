#ifndef TOOLS_H
#define TOOLS_H

// Définition Structure Pour Instrument
typedef struct 
{
    char No_Instrument [8]; // Exemple : FIT-101  * No numéro Unique
    int Range_High; // Exemple : 95
    int Range_Low; // Exemple : 10
    char Unit [5]; // Kpa , m3/h , % etc...
}S_Instrument;

// Definition Structure pour CelluleInstrument

struct CelluleInstrument
{
S_Instrument Instrument;
struct CelluleInstrument *suivant;
};
typedef struct CelluleInstrument S_CelluleInstrument;


// Définition Structure Pour Ctrl
typedef struct 
{
   char No_Ctrl [8]; //Exemple PLC-101  * Numéro Unique
   char No_Secteur [5]; // B100
   char Fabricant [3]; // Pour Allen-Bradley = AB , pour DeltaV = DV;
   int Version ; // 1 , 2 ,3 etc... 
   int EspaceTotal ;
   int EspaceDisponible;
}S_Ctrl;

// Definition Structure pour CelluleCtrl

struct CelluleCtrl {
S_Ctrl Ctrl;;
S_CelluleInstrument * Instruments;
struct CelluleCtrl *suivant;
};
typedef struct CelluleCtrl S_CelluleCtrl;

// Definition Structure Pour CelluleCtrlIntstrument

struct CelluleCtrlInstrument {
char ID_Instrument [8];  // utiliser pour la fonction Load, identique à No_Instrument
char ID_Ctrl [8]; // 
char ID_Secteur [5];
S_CelluleInstrument *Instrument;
struct CelluleCtrlInstrument *suivant;
};

typedef struct CelluleCtrlInstrument S_CelluleCtrlInstrument;


//prototype de fonction

void F_AddNewCtrl (S_CelluleCtrl **);
S_CelluleCtrl * F_CreateCelluleCtrl();
void F_UpdateCelluleCtrl(S_Ctrl , S_CelluleCtrl *);
void F_AddCelluleCtrl(S_CelluleCtrl **,S_CelluleCtrl *);
S_CelluleInstrument * F_CreateCelluleInstrument();
void F_updateCelluleInstrument(S_Instrument , S_CelluleInstrument *);
void F_addCelluleInstrument(S_CelluleInstrument **, S_CelluleInstrument *);
void F_PrintCelluleCtrl(S_CelluleCtrl );
void F_PrintCelluleCtrlList(S_CelluleCtrl *);
void F_printCelluleInstrument(S_CelluleInstrument );
void F_UpdateCelluleInstrument(S_Instrument, S_CelluleInstrument *);
void F_AddCelluleInstrument(S_CelluleInstrument **, S_CelluleCtrlInstrument * );
int F_ExistCtrl(S_CelluleCtrl *, char *, char *);
void F_InstrumentToCtrl(S_CelluleCtrl *,S_CelluleInstrument**);
void F_AddInstrumentToCtrl(S_CelluleCtrl *,S_CelluleInstrument**, char *, char *);
void F_PrintCelluleInstrumentlist(S_CelluleCtrl *);
int F_addActiveInstrumentToCtrl(S_CelluleCtrlInstrument ** addrInstrumentCtrlActif,S_CelluleInstrument *activeInstrument, char* , char*, char*);

int F_existInstrument(S_CelluleInstrument *t, char *);

void F_save(S_CelluleCtrl* , S_CelluleInstrument* );
#endif