#include<stdio.h>
#include "../src/tools.h"
#include <stdlib.h>

void main() 
{
    
    // Déclaration des variables 

    char V_Choix_Menu;
    int V_Choix_Afficher;
    int V_Choix_Ajouter;
    S_CelluleInstrument *listeInstrument = NULL;
    S_CelluleCtrl *listeCtrl =NULL ;
    
    

   // printf("Ajout d'un Ctrl dans BD , Appuyez sur Entrée pour continuer...");
   // F_AddNewCtrl(&listeCtrl);
   // printf("Appuyez sur Entrée pour revenir au menu...");
   // F_PrintCelluleCtrlList(listeCtrl);

   //return;

   do {
             system("clear");
             printf("****************************************************************************************************************\n");
             printf("*                                                     Menu Principal                                           *\n");                                                                                                        
             printf("*                         Q : Quitter | A : Afficher Data  | N :  Ajouter Data    | S :  Save  DB              *\n");
             printf("****************************************************************************************************************\n");
             printf("Entrez votre choix: "); 
             scanf(" %c", &V_Choix_Menu);
             switch (V_Choix_Menu) {
                case 'Q':
                case 'q':
                    printf("Vous avez choisi de quitter.\n");
                    break;
                case 'A':
                case 'a':
                    do
                    {
                        system("clear");
                        printf("****************************************************************************************************************\n");
                        printf("*                                         Visualiation DATA                                                    *\n");
                        printf("*                                     1 : Retour Menu Principal                                                *\n");
                        printf("*                                     2 : Contrôleurs dans Data Base                                           *\n");
                        printf("*                                     3 : Instrument dans Date Base                                            *\n");
                        printf("****************************************************************************************************************\n");
                        printf("Entrez votre choix: ");    
                        scanf(" %d", &V_Choix_Afficher);
                            switch (V_Choix_Afficher) 
                                    {
                                        case 1:                
                                            break;
                                        case 2:                                                                                     
                                            if (listeCtrl == NULL) 
                                            {
                                                system("clear");                                               
                                                printf("***********************************************************************************************************\n");
                                                printf("*                                       La liste est vide                                                 *\n");
                                                printf("***********************************************************************************************************\n");
                                            }                                          
                                            else                                         
                                            {
                                                system("clear");
                                                F_PrintCelluleCtrlList(listeCtrl);
                                            }
                                            printf("Appuyez sur Entrée pour continuer...");
                                            getchar(); 
                                            getchar();
                                            system("clear");
                                            break;
                                        
                                        case 3:
                                            if (listeInstrument == NULL)
                                            {
                                                system("clear");                                               
                                                printf("****************************************************************************************************************\n");
                                                printf("*                                       La liste est vide                                                      *\n");
                                                printf("****************************************************************************************************************\n");
                                            }                                          
                                            else                                         
                                            {
                                                system("clear");
                                                F_PrintCelluleInstrumentlist(listeInstrument);
                                            }
                                            printf("Appuyez sur Entrée pour continuer...");
                                            getchar(); 
                                            getchar(); 
                                            system("clear");
                                            break;
                                        default:
                                            printf("Choix non valide.\n");
                                            printf("Appuyez sur Entrée pour continuer...");
                                            getchar(); 
                                            getchar(); 
                                            break;
                                    }                                      
                    } while (V_Choix_Afficher != 1); 
                 break;  
                case 'N':
                case 'n':
                    do
                    {
                        system("clear");
                        printf("****************************************************************************************************************\n");
                        printf("*                                            Modification DATA                                                 *\n");
                        printf("*                                        1 : Retour Menu Principal                                             *\n");
                        printf("*                                        5 : Ajout Ctrl dans la DB                                             *\n");
                        printf("*                                        6 : Ajout Instrument à un Ctrl                                        *\n");
                       printf("****************************************************************************************************************\n");
                        printf("Entrez votre choix: ");    
                        scanf(" %d", &V_Choix_Ajouter);
                            switch (V_Choix_Ajouter) 
                                    {
                                        case 1:                
                                            break;
                                        case 5:
                                            printf("Ajout d'un Ctrl dans BD , Appuyez sur Entrée pour continuer...");
                                            getchar(); 
                                            getchar(); 
                                            F_AddNewCtrl(&listeCtrl);
                                            printf("Appuyez sur Entrée pour revenir au menu...");
                                            getchar(); 
                                            getchar();
                                            system("clear");
                                            break;                                       
                                        case 6:
                                            printf("Ajout d'un Instrument à un Ctrl , Appuyez sur Entrée pour continuer...");
                                            getchar();
                                            getchar();
                                            F_InstrumentToCtrl(listeCtrl,&listeInstrument);
                                            printf("Appuyez sur Entrée pour revenir au menu...");
                                            getchar(); 
                                            getchar(); 
                                            system("clear");
                                            break;
                                        default:
                                            printf("Choix non valide.\n");
                                            printf("Appuyez sur Entrée pour continuer...");
                                            getchar(); 
                                            getchar(); 
                                            break;
                                    }                                      
                    } while (V_Choix_Ajouter != 1);  
                 break; 
                case 'S':
                case 's':
                    printf("Pour Sauvegarder les données Appuyez sur Entrée...");
                    getchar(); 
                    getchar(); 
                    F_save(listeCtrl,listeInstrument); 
                    printf("Appuyez sur Entrée pour continuer...");
                    getchar(); 
                    getchar();
                    system("clear");
                    break;  

                case 'Z':
                case 'z':
                    S_CelluleCtrl *p = listeCtrl;
                    printf("ptr instrument %p \n",p->Instruments->Instrument);
                    printf("ctrl %s\n",p->Ctrl.No_Ctrl);
                    for (int i = 0; i < 1; i++)
                    {
                    printf("instrument %s\n",p->Instruments->Instrument.No_Instrument);
                    }
                                   
                    getchar();
                    getchar();
                    break;
                }  
    } while (V_Choix_Menu != 'Q' && V_Choix_Menu != 'q');  


} 